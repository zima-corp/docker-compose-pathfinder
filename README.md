# Example for running a Pathfinder in the docker container

> **!!! Disclamer !!!**
>
> This configuration is a copy of the development environment
> in which sensitive data has been replaced.  
> It does not work!  
> This is an example of how to configure Pathfinder.

Differences from the [github.com/goryn-clade/pathfinder-containers](https://github.com/goryn-clade/pathfinder-containers) project:

- Uses php 7.4
- Uses [nginx unit](https://unit.nginx.org/) instead of nginx + php-fmp.
- Used [supercronic](https://github.com/aptible/supercronic) to perform periodic tasks in a separate service

Repositories with the Pathfinder application:

- [exodus4d/pathfinder](https://github.com/exodus4d/pathfinder)
- [goryn-clade/pathfinder](https://github.com/goryn-clade/pathfinder)
- [jithatsonei/pathfinder](https://github.com/jithatsonei/pathfinder)

Docker containers:

- [gitlab.com/zima-corp/docker-pathfinder-base](https://gitlab.com/zima-corp/docker-pathfinder-base)
- [gitlab.com/zima-corp/docker-pathfinder-websocket](https://gitlab.com/zima-corp/docker-pathfinder-websocket)
- [gitlab.com/zima-corp/docker-pathfinder](https://gitlab.com/zima-corp/docker-pathfinder)
